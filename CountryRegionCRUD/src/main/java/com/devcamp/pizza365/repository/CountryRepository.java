package com.devcamp.pizza365.repository;

import com.devcamp.pizza365.model.CCountry;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<CCountry, Long> {
	Optional<CCountry> findByCountryCode(String countryCode);
	Optional<CCountry> findByCountryName(String countryName);
}
